Articular::Engine.routes.draw do
  get ':year/:month/:id' => 'articles#show', :as => :article

  resources :articles, :only => [:index], :path => '' do
    get ':year/:month' => 'articles#archive', :on => :collection, :as => :archive
  end
end
