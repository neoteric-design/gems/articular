require 'spec_helper'

describe Articular::Controller do
  let(:request) { OpenStruct.new(path: '/hello/world') }
  let(:news_post) { double(title: 'Hello World') }
  let(:controller) { MockPostsController.new }
  let(:published_posts) { double }

  before :each do
    allow(controller).to receive(:request) { request }
    allow(controller).to receive(:render)
    allow(controller).to receive(:respond_with)
    allow(controller).to receive(:params) { { page: 1, month: 8, year: 2010 } }
  end

  describe 'GET index' do
    it 'assigns published posts' do
      expect(MockPost).to receive(:published)
        .and_return(published_posts)

      expect(published_posts).to receive(:page).with(1)

      controller.index
    end
  end

  describe 'GET show' do
    it 'assigns the requested post' do
      expect(MockPost).to receive_message_chain(:published, :friendly, :find)
        .with('hello-world').and_return(news_post)

      allow(controller).to receive(:params) { { id: 'hello-world' } }
      controller.show
    end
  end

  describe 'GET archive' do
    it 'grabs posts by month and year' do
      expect(MockPost).to receive_message_chain(:published, :page)
        .and_return(published_posts)
      expect(published_posts).to receive(:from_archive).with(2010, 8)

      controller.send(:archive)
    end

    it 'renders index' do
      expect(MockPost).to receive_message_chain(:published, :page, :from_archive)
      expect(controller).to receive(:render).with(:index)

      controller.send(:archive)
    end
  end
end
