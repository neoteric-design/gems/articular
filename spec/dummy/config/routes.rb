Rails.application.routes.draw do
  scope 'mock_posts' do
    root to: 'mock_posts#index', as: :mock_posts
    get 'tagged/:tag' => 'mock_posts#tagged', :as => :tagged_mock_posts
    get ':year/:month/:id' => 'mock_posts#show'
    get ':year(/:month)' => 'mock_posts#archive', :as => :archive_mock_posts
  end
end
