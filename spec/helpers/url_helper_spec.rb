require 'spec_helper'

module Articular
  RSpec.describe UrlHelper, type: :helper do
    describe 'article_path' do
      context 'existing article' do
        let(:subject) do
          MockPost.create(title: 'The', published_at: Time.new(2017, 5, 5))
        end

        it 'builds out url_for controller/show/year/month/slug' do
          expect(helper.article_path(subject)).to eq('/mock_posts/2017/5/the')
        end
      end

      context 'preview/unsaved article' do
        let(:subject) { MockPost.new }
        it 'returns url for index' do
          expect(helper.article_path(subject)).to eq('/mock_posts')
        end
      end
    end
  end
end
