class CreateArticles < ActiveRecord::Migration[5.0]
  def change
    create_table :articular_articles do |t|
      t.string :title
      t.text :excerpt
      t.text :body
      t.datetime :published_at, :index => true
      t.string :state, :null => false,
                       :default => 'published',
                       :index => true
      t.string :slug, :index => true
      t.string :type, :index => true
      t.string :meta_description, :text

      t.timestamps null: false
    end
  end
end
