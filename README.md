# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 

--------------------

# Articular

News engine for the Neoteric Design CMS.

## Installation

Neoteric CMS gems are served from a private host. Replace `SECURE_GEMHOST` with the source address.

```ruby
# Gemfile
source SECURE_GEMHOST do
  gem 'articular', '~> 2.4.1'
end
```

```sh
$ bundle install
$ rails g articular:install
$ rake db:migrate
```

The install generator:
* Sets up the articular_articlew table
* Bootstraps active_admin integration if ActiveAdmin is detected
* Sets up an initial NewsPost resource 

You can also run the active_admin bootstrapper independently. This adds the
appropriate JS/CSS needed for Articular's admin UI.


```
$ rails g articular:active_admin
```

You can generate additional post resources.

```
$ rails g articular:resource CoolBlogPost
```

This generates:
* Model
* Model spec
* Controller
* Views/RSS feed
* Routes
* ActiveAdmin resource (if activeadmin present)



## Usage

Out of the box, you'll get a NewsPost model and a corresponding controller


### Model
```ruby
class NewsPost < Articular::Article
end
```

#### Instance methods

| Attribute/method       | Description                                |
|------------------------|--------------------------------------------|
|`title`                 |Article title                               |
|`excerpt`               |Plaintext (or lightly styled) excerpt       |
|`state`                 |'drafted' or 'published' state              |
|`published_at`          |Datetime of publishing, by default future articles are private|
|`meta_description`      |For page's meta tags                        |
|`body`                  |Article body (when not using _Blocks_)      |
|------------------------|--------------------------------------------|
|`next`                  | Next published article, chronologically    |
|`prev`                  | Previous published article, chronologically|


#### Class methods

| Scope/method           | Description                                |
|------------------------|--------------------------------------------|
|`newest_first`          | **Default scope** published_at descending  |
|`oldest_first`          | published_at ascending                     |
|`published`             | Not drafted, present                       |
|`drafted`               | Drafted or future                          |
|`future`                | Published after now                        |
|`present`               | Published on or before now                 |
|`by_month(year, month)` | All posts with published_at in given month |
|`by_year(year)`         | All posts with published_at in given year  |


### Controller
```ruby
class AnyNameController < ApplicationController
  include Articular::Controller
end
```

Articular will guess what class it's working with based on the controller's name, but this can be overridden. Most of the operations like generating the title and pagination have been broken down into small methods so they can be easily overriden.  See [lib/articular/controller.rb](lib/articular/controller.rb) for what's under the hood.

In addition to these methods, feel free to replace the actions according to your own needs.



### View Helpers

Because of the fancy date-based routes, Articular includes some url helpers for generating links to articles. It does the reflection on the article class and date for you and uses Rails' `url_for` under the hood that should work in most instances. You can override the optional options hash if it doesn't.

```ruby
article_path(news_post)
#=> "/news/2016/04/cotton-eye-joe"

article_url(blog_post)
#=> "http://mydomain.com/blog/2016/10/31/legend-of-spooky-pete"
```



### Tags

Ships with support for Neoteric Design _Topical_ tagging engine.

```ruby
class NewsPost < Articular::Article
  topical_on :news_topics
end
```

To use a different tagging method, remove that reference from the model, and override the `tagged` action in your controller.

### Admin/editor support

Articular comes with (hopefully) everything you need to set up an admin interface. Articular can generate one for you when you're installing.

* Assets: Include `articular.js` and `articular.scss`
* Strong parameters: adding `Articular.params` to the whitelist

#### ActiveAdmin Example
```coffeescript
# app/assets/javascripts/active_admin.js.coffeescript

# ...
#= require articular
# ...
```

```scss
// app/assets/stylesheets/active_admin.css.scss

// ...
@import 'articular';
// ...
```

```ruby
# app/admin/news_posts.rb

ActiveAdmin.register NewsPost do
  menu :priority => 2
  actions :all, :except => :show
  config.sort_order = 'published_at DESC'

  permit_params Articular.params, news_topic_ids: []

  filter :title
  filter :body
  filter :published_at

  scope :all
  scope :published

  form :partial => 'form'

  index do
    column :title
    column :state do |post|
      span post.state, :class => "state-label #{post.state}"
    end
    column :published_at

    actions
  end

  controller do
    def find_resource
      scoped_collection.friendly.find(params[:id]) #FriendlyId Find
    end
  end
end
```
See also the [default ActiveAdmin register template](lib/generators/articular/install/templates/admin_articles.rb.erb).


## Testing

A RSpec linting module is available to double check functionality.

```ruby
describe NewsPost do
  include Articular::SpecHelpers::Lint
end
```

## TODO

* [] Rails generator for adding new article models
* [] Documentation for Archive and Navigator modules




