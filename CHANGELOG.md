2.5.0
=====

* Ruby 3 fixes

2.4.10
======

* Fix type coercion for Date in Ruby 2.7

2.4.9
=====

* Add Rails 6 compatibility

2.4.7, 2.4.8
============

* Fix order queries for certain live preview situations

2.4.6
=====

* Fix issue with published? method when published_at timestamp not present

2.4.5
=====

* Add published? and drafted? instance methods to complement class scopes

2.4.4
=====

* Further refine/fix invalid date handling from 2.4.3.

2.4.3
=====

* Invalid args to archive scopes now raise a `Articular::DateError`. This is
  rescued by default with a redirect to index
* New method Articular.date_constraints help contain route collisions by only
  matching digits for year/month to archive route

2.4.2
=====

* Reorganize generators for install, article resources, and activeadmin
* Move article form to gem's views

2.4.1
======

* Include updated specs for datetime published_at

2.4.0
=====

* Use OrderQuery for consistent navigation from article to article
* Use datetime to store published_at

2.3.0
=====

* Loosen dependencies
* Fix: tags admin page template
* New: `::archive_months` now returns an array of ArchiveMonth objects, rather
  than confusing hashes
* Remove will_paginate in favor of Kaminari, because other things already depend
  on Kaminari.* Remove will_paginate in favor of Kaminari, whi because other
  things already depend on Kaminari.* Remove will_paginate in favor of Kaminari,
  because other things already depend on Kaminari.* Remove will_paginate in
  favor of Kaminari, because other things already depend on Kaminari.* Remove
  will_paginate in favor of Kaminari, because other things already depend on
  Kaminari.* Remove will_paginate in favor of Kaminari, w because other things
  already depend on Kaminari.* Remove will_paginate in favor of Kaminari, w
  because other things already depend on Kaminari.* Remove will_paginate in
  favor of Kaminari, because other things already depend on Kaminari.

2.2.1
=====

* Fixed: Type error when passing year or month as strings to `Article::from_archive`
* Code Health: Spin out spec helpers for Archiver, Navigator, and Model to reflect their current separation in code.

2.2.0
=====

* Keep Model mixin but use it to establish a real Article base class to
  follow proper Rails STI behavior and provide access to anything in the articles table
* Do a massive pare down of the archiver and navigator modules. Remove some of
  the metaprogramming and seldom used and over-engineered features. They
  should be easier to grok and peform better

2.1.0
=====

* Use composition rather than inheritance to build article models
* Move UrlHelper to app/helpers where Rails will autoload it for us
* Fix order of tagged article routes so they are once again matched appropriately
* Add Articular.params method for convenience and parity with other CMS gems

2.0.0
=====

* Support Rails 5
* Remove dependency on pubdraft
