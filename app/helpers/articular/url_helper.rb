module Articular
  module UrlHelper
    def article_path(article, **options)
      params = { controller: "/#{article.model_name.plural}" }
      params[:action] = article.persisted? ? :show : :index
      params.merge!(article.to_fancy_param)

      url_for params.merge(options)
    end

    def article_url(article, **options)
      article_path article,
                   **Rails.application.routes.default_url_options.merge(options)
    end

    def archive_articles_path(date, **options)
      defaults = { action: 'archive', year: date.year, month: date.month }
      url_for defaults.merge(options)
    end

    def tagged_articles_path(tag_id, **options)
      defaults = { action: 'tagged', tag: tag_id }
      url_for defaults.merge(options)
    end
  end
end
