module Articular
  class Article < ApplicationRecord
    include Model
    self.table_name = "articular_articles"
  end
end
