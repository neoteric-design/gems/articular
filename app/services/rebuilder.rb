module Rebuilder
  def self.included(base)
    base.send(:collection_action, :rebuild, :method => :post) do
      params[param_name].each_with_index do |id, index|
        resource_class.update(id, position: index + 1)
      end

      render nothing: true
    end

    base.controller do
      private
      def param_name
        # p active_admin_config.resource_label.downcase.gsub(' ', '_').to_sym
        resource_class.model_name.to_s.underscore.gsub('/', '_').to_sym
      end
    end

  end
end
