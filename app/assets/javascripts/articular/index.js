//= require select2
//= require ./sortable_table

$(function() {
  $('.select2').select2();

  $("table#index_table_news_topics").sortable_table();
});