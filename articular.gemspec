$:.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'articular/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'articular'
  s.version     = Articular::VERSION
  s.authors     = ['Joe Sak', 'Nic Aitch', 'Madeline Cowie']
  s.email       = ['joe@joesak.com', 'nic@nicinabox.com', 'madeline@cowie.me']
  s.homepage    = 'http://neotericdesign.com'
  s.summary     = 'Articles gem by Neoteric Design'
  s.description = 'Not much to describe . . .'

  s.files = Dir['{app,config,db,lib}/**/*'] + ['Rakefile', 'README.md']

  s.add_dependency 'rails', '>= 5.0.0'
  s.add_runtime_dependency 'topical', '~> 0.1'

  s.add_runtime_dependency 'friendly_id', '~> 5.0', '>= 5.0.3'
  s.add_runtime_dependency 'kaminari'
  s.add_runtime_dependency 'order_query'
  s.add_runtime_dependency 'responders', '>= 2.2', '< 4.0'
  s.add_runtime_dependency 'select2-rails', '~> 4.0'

  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'pg'
  s.add_development_dependency 'rspec-rails', '~> 3.5'
  s.add_development_dependency 'timecop'
end
