require 'rails/generators'

module Articular
  class ResourceGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    argument :model_name, :type => :string, :default => "NewsPost"

    def copy_templates
      pluralized_name = model_name.tableize
      singular_name = model_name.underscore

      template "admin_resource.rb.erb", "app/admin/#{singular_name}.rb" if defined?(ActiveAdmin)
      template "model.rb.erb", "app/models/#{singular_name}.rb"
      template "controller.rb.erb", "app/controllers/#{pluralized_name}_controller.rb"

      template "index.rss.builder.erb", "app/views/#{pluralized_name}/index.rss.builder"
      template "index.html.erb", "app/views/#{pluralized_name}/index.html.erb"
      template "show.html.erb", "app/views/#{pluralized_name}/show.html.erb"

      template "partial.html.erb", "app/views/#{pluralized_name}/_#{singular_name}.html.erb"
      template "sidebar.html.erb", "app/views/#{pluralized_name}/_sidebar.html.erb"
      template "header.html.erb", "app/views/#{pluralized_name}/_header.html.erb"
    end

    def configure_routes
      truncated_name = model_name.underscore.pluralize.gsub('_posts','')
      pluralized_name = model_name.tableize
      route <<~ROUTES
      scope '#{truncated_name}' do
          root to: '#{pluralized_name}#index', as: :#{pluralized_name}
          get 'tagged/:tag' => '#{pluralized_name}#tagged', :as => :tagged_#{pluralized_name}
          get ':year/:month/:id' => '#{pluralized_name}#show', constraints: Articular.date_constraints
          get ':year(/:month)' => '#{pluralized_name}#archive', :as => :archive_#{pluralized_name}, constraints: Articular.date_constraints
        end
      ROUTES
    end
  end
end
