require 'rails/generators'

module Articular
  class InstallGenerator < Rails::Generators::Base
    source_root File.expand_path("../templates", __FILE__)
    argument :model_name, :type => :string, :default => "NewsPost"

    def copy_templates
      copy_file "articular.rb", "config/initializers/articular.rb"
    end

    def install_topical
      generate "topical:install"
    end

    def install_migrations
      rake "articular:install:migrations"
    end

    def generate_admin
      generate 'articular:active_admin' if defined?(ActiveAdmin)
    end

    def generate_resource
      generate 'articular:resource', @model_name
    end
  end
end
