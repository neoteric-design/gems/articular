require 'order_query'

module Articular
  # Scope and move through a model based on a designated column
  module Navigator
    def self.included(base)
      base.extend ClassMethods
      base.include InstanceMethods
      base.include ::OrderQuery

      base.send :order_query, :newest_first,
                [base.navigator_field, :desc, nulls: :last],
                [:created_at, :desc, nulls: :last]
                [:id, :desc, nulls: :last]
      base.send :order_query, :oldest_first,
                [base.navigator_field, :asc, nulls: :last],
                [:created_at, :asc, nulls: :last],
                [:id, :asc, nulls: :last]
    end

    # :nodoc:
    module ClassMethods
      def navigator_field
        :published_at
      end

      def future
        where("#{navigator_field} > ?", Time.current)
      end

      def present
        where("#{navigator_field} <= ?", Time.current)
      end
    end

    # :nodoc:
    module InstanceMethods
      def navigation_point(collection = nil)
        collection ||= self.class.unscoped
        oldest_first(collection)
      end

      def next(collection = self.class.unscoped.published)
        navigation_point(collection).next(false)
      end

      def prev(collection = self.class.unscoped.published)
        navigation_point(collection).previous(false)
      end
    end
  end
end
