module Articular
  module Controller
    def self.included(base)
      base.respond_to :html
      base.respond_to :rss, only: :index

      base.rescue_from 'Articular::DateError', with: :on_invalid_date
    end

    def index
      instance_variable_set "@#{model_name.plural}", scoped_collection

      @title = index_title
      respond_with instance_variable_get("@#{model_name.plural}")
    end

    def show
      instance_variable_set "@#{model_name.singular}",
      article_class.published.friendly.find(params[:id])

      @title = show_title
    end

    def tagged
      find_topic
      instance_variable_set "@#{model_name.plural}",
                            scoped_collection.tagged_with(@topic)

      @title = tagged_title
      render :index
    end

    def archive
      instance_variable_set "@#{model_name.plural}",
                            scoped_collection.from_archive(year, month)

      @title = archive_title
      render :index
    end

    private

    def scoped_collection
      article_class.published.page(params[:page])
    end

    # Basic name of resource
    def base_title
      article_class.name.pluralize
    end

    # Title for index action
    def index_title
      base_title
    end

    # Title for show action
    def show_title
      article_title = instance_variable_get("@#{model_name.singular}").title
      "#{article_title} - #{base_title}"
    end

    # Title for tagged action
    def tagged_title
      "#{base_title} posted under #{@topic.name}"
    end

    # Title for archive action
    def archive_title
      "#{base_title} posted in #{month_name} #{year}"
    end

    # Model to operate with
    def article_class
      self.class.name.gsub('Controller', '').singularize.constantize
    end

    def model_name
      article_class.model_name
    end

    # Tag retrieval for tagged action
    def find_topic
      @topic = Topical::Tag.find_by_slug(tag_context, params[:tag])
      raise ActiveRecord::RecordNotFound unless @topic
    end

    # Context for Topical lookup
    def tag_context
      :news_topics
    end

    def year
      params[:year].to_i if params[:year].present?
    end

    def month
      params[:month].to_i if params[:month].present?
    end

    def month_name
      Date::MONTHNAMES[month] if month.present?
    end

    def on_invalid_date
      logger.warn "Invalid archive params: #{params.slice(:year, :month)}"
      redirect_to action: :index
    end
  end
end
