require 'kaminari'
require 'friendly_id'
require 'topical'
require 'select2-rails'

require 'articular/date_error'
require 'articular/navigator'
require 'articular/archiver'

require 'articular/controller'
require 'articular/model'

require 'articular/spec_helpers'

module Articular
  class Engine < ::Rails::Engine
    isolate_namespace Articular

    initializer 'articular.action_controller',
                :before => :load_config_initializers do
      ActiveSupport.on_load :action_controller do
        ::ActionController::Base.send(:helper, Articular::UrlHelper)
      end
    end
  end
end
