module Articular
  module Model
    def self.included(base)
      base.extend ClassMethods
      base.include InstanceMethods
      base.include Articular::Archiver
      base.include Articular::Navigator
      base.extend FriendlyId

      base.friendly_id :title, use: :slugged
      base.before_save :set_published_at
      base.paginates_per 10
    end

    module InstanceMethods
      def should_generate_new_friendly_id?
        title_changed?
      end

      def to_fancy_param
        return {} unless persisted?
        {
          year: published_at.year,
          month: published_at.month,
          id: slug
        }
      end

      def published?
        published_at.present? &&
          published_at <= Time.current &&
          state == 'published'
      end

      def drafted?
        !published?
      end

      private

      def date_and_title
        [published_at.to_s, title].join(' ')
      end

      def set_published_at
        return unless published_at.blank? && state == 'published'
        self.published_at = Time.zone.now
      end
    end

    module ClassMethods
      def default_scope
        newest_first
      end

      def published
        present.where(state: 'published')
      end

      def drafted
        future.or(where(state: 'drafted'))
      end
    end
  end
end
