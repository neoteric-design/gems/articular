module Articular
  # Value object for holding a month in history along with article count
  class ArchiveMonth
    attr_reader :date, :count

    def initialize(date:, count:)
      @date  = date
      @count = count
    end

    def to_s
      "#{date.strftime('%B %Y')} (#{count})"
    end

    def ==(other)
      date == other.date && count && other.count
    end
  end
end
