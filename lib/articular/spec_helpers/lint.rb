module Articular
  module SpecHelpers
    module Lint
      def self.included(base)
        base.include ArticleLint
        base.include ArchiverLint
        base.include NavigatorLint
      end
    end
  end
end
