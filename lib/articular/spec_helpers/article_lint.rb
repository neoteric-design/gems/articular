module Articular
  module SpecHelpers
    module ArticleLint
      def self.included(base)
        base.describe 'Article Model' do
          describe 'inheritance' do
            it 'sets up rails STI types' do
              expect(subject.class.new.type).to eq(subject.class.name)
            end
          end

          def today
            @today ||= Time.zone.now
          end

          def yesterday
            today - 1.day
          end

          def tomorrow
            today + 1.day
          end

          it 'sorts all posts by published_at, created_at DESC' do
            subject.class.destroy_all

            old = subject.class.create!(title: 'old', state: 'published',
                                        published_at: yesterday)

            fresh = subject.class.create!(title: 'fresh', state: 'published',
                                          published_at: today)

            freshest = subject.class.create!(title: 'freshest', state: 'published',
                                          published_at: today)

            expect(subject.class.all).to match [freshest, fresh, old]
          end

          it 'sets published_at on state = published' do
            post = subject.class.create!
            post.update(state: 'published')

            expect(post.published_at.day).to eq today.day
          end

          it 'does not set publish_at if it has a value' do
            post = subject.class.create!
            post.update(published_at: yesterday,
                                   state: 'published')

            expect(post.published_at).to eq yesterday
          end

          it 'allows a meta_description' do
            post = subject.class.create!(meta_description: 'Check this out')
            expect(post.meta_description).to eq 'Check this out'
          end

          describe 'scopes' do
            let!(:published_today) do
              subject.class.create!(published_at: today,
                                    state: 'published')
            end

            let!(:published_yesterday) do
              subject.class.create!(published_at: yesterday,
                                    state: 'published')
            end

            let!(:published_tomorrow) do
              subject.class.create!(published_at: tomorrow,
                                    state: 'published')
            end

            let!(:drafted) do
              subject.class.create!(state: 'drafted')
            end

            describe '#published' do
              it 'includes all published articles today or earlier' do
                expect(subject.class.published).to include(published_today)
                expect(subject.class.published).to include(published_yesterday)
              end

              it 'does not include published articles in the future' do
                expect(subject.class.published).to_not include(published_tomorrow)
              end

              it 'does not include drafted articles' do
                expect(subject.class.published).to_not include(drafted)
              end
            end

            describe '#drafted' do
              it 'includes all drafted or future articles' do
                expect(subject.class.drafted).to include(drafted)
                expect(subject.class.drafted).to include(published_tomorrow)
              end

              it 'does not include published articles' do
                expect(subject.class.drafted).to_not include(published_today)
                expect(subject.class.drafted).to_not include(published_yesterday)
              end
            end

            describe '#future' do
              it 'includes all articles set in the future' do
                expect(subject.class.future).to include(published_tomorrow)
              end

              it 'does not include past or present articles' do
                expect(subject.class.future).to_not include(published_today)
                expect(subject.class.future).to_not include(published_yesterday)
              end
            end

            describe '#present' do
              it 'does not include any articles set in the future' do
                expect(subject.class.present).to_not include(published_tomorrow)
              end

              it 'includes past or present articles' do
                expect(subject.class.present).to include(published_today)
                expect(subject.class.present).to include(published_yesterday)
              end
            end
          end
        end
      end
    end
  end
end
