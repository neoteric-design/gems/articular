module Articular
  module SpecHelpers
    # Linting for models with Archiver
    module ArchiverLint
      def self.included(base)
        base.describe 'Archiver' do
          let(:archiver_field) { subject.class.archiver_field }

          describe '.archive_months' do
            before :each do
              subject.class.delete_all

              subject.class.create!(archiver_field => Date.new(2011, 1, 18))
              subject.class.create!(archiver_field => Date.new(2011, 1, 17))
              subject.class.create!(archiver_field => Date.new(2011, 1, 16),
                                    state: 'drafted')
              subject.class.create!(archiver_field => Date.new(2011, 3, 3),
                                    state: 'drafted')
              subject.class.create!(archiver_field => Date.new(2016, 3, 12))
            end

            it 'returns month starts and counts' do
              expected = [
                ArchiveMonth.new(date: Time.utc(2011, 1), count: 3),
                ArchiveMonth.new(date: Time.utc(2011, 3), count: 1),
                ArchiveMonth.new(date: Time.utc(2016, 3), count: 1)
              ]

              expect(subject.class.archive_months).to eq(expected)
            end

            it 'can be scoped to published' do
              expected = [
                ArchiveMonth.new(date: Time.utc(2011, 1), count: 2),
                ArchiveMonth.new(date: Time.utc(2016, 3), count: 1)
              ]
              expect(subject.class.published.archive_months).to eq(expected)
            end
          end

          describe 'Lookups' do
            let!(:august10) do
              subject.class.create!(archiver_field => Date.new(2011, 8, 10))
            end

            let!(:july1) do
              subject.class.create!(archiver_field => Date.new(2010, 7, 1))
            end

            let!(:august1) do
              subject.class.create!(archiver_field => Date.new(2010, 8, 1))
            end

            describe '.by_month' do
              let(:posts) { subject.class.by_month(2010, 8) }

              it 'returns posts from a given month' do
                expect(posts).to include(august1)
              end

              it 'excludes posts from the same month but different year' do
                expect(posts).to_not include(august10)
              end

              it 'excludes posts from other months' do
                expect(posts).to_not include(july1)
              end


              context 'bad arguments' do
                it 'raises a DateError' do
                  expect { subject.class.by_month(2010, 13) }.to raise_exception(Articular::DateError)
                  expect { subject.class.by_month('fff', 12) }.to raise_exception(Articular::DateError)
                end
              end
            end

            describe '.by_year' do
              let(:posts) { subject.class.by_year(2010) }

              it 'returns posts from a given year' do
                expect(posts).to include(august1, july1)
                expect(posts).to_not include(august10)
              end

              context 'bad arguments' do
                it 'raises a DateError' do
                  expect { subject.class.by_year('ffff') }.to raise_exception(Articular::DateError)
                end
              end
            end
          end
        end
      end
    end
  end
end
