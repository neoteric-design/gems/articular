module Articular
  module SpecHelpers
    module NavigatorLint
      def self.included(base)
        base.describe 'Navigator' do
          let(:central_date) { Date.new(2016, 10, 1) }
          let(:navigator_field) { subject.class.navigator_field }

          let!(:post) do
            subject.class.create! title: 'post',
                                  navigator_field => central_date
          end

          let!(:drafted_newer_post) do
            subject.class.create! title: 'drafted_newer_post',
                                  navigator_field => (central_date + 1.day),
                                  state: 'drafted'
          end

          let!(:newer_post) do
            subject.class.create! title: 'newer_post',
                                  navigator_field =>  (central_date + 2.days)
          end

          let!(:newest_post) do
            subject.class.create! title: 'newest_post',
                                  navigator_field =>  (central_date + 1.year)
          end

          let!(:drafted_older_post) do
            subject.class.create! title: 'drafted_older_post',
                                  navigator_field =>  (central_date - 1.day),
                                  state: 'drafted'
          end

          let!(:older_post) do
            subject.class.create! title: 'older_post',
                                  navigator_field =>  (central_date - 2.days)
          end

          let!(:oldest_post) do
            subject.class.create! title: 'oldest_post',
                                  navigator_field =>  (central_date - 1.year)
          end

          it '::navigator_field' do
            expect(subject.class.navigator_field).to be_present
          end

          describe '#next' do
            it 'tie breaks by created_at' do
              same_time_post = subject.class.create!(navigator_field => central_date, created_at: post.created_at + 1.second)
              expect(post.next).to eq(same_time_post)
              expect(same_time_post.next).to eq(newer_post)
            end

            it 'returns the next published post by newer date' do
              expect(post.next).to eq(newer_post)
            end

            it 'can be unscoped' do
              expect(post.next(nil)).to eq(drafted_newer_post)
            end

            it 'returns nil if there are none' do
              expect(newest_post.next).to be_nil
            end
          end

          describe '#prev' do
            it 'tie breaks by created_at then ID' do
              same_time_post = subject.class.create!(navigator_field => central_date,
                                                     created_at: post.created_at + 1.second)
              expect(same_time_post.prev).to eq(post)
              expect(post.prev).to eq(older_post)
            end

            it 'returns the previous published post by older date' do
              expect(post.prev).to eq(older_post)
            end

            it 'can be unscoped' do
              expect(post.prev(nil)).to eq(drafted_older_post)
            end

            it 'returns nil if there are none' do
              expect(oldest_post.prev).to be_nil
            end
          end
        end
      end
    end
  end
end
