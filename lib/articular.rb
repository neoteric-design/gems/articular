require "articular/engine"

module Articular
  def self.params
    [:body, :excerpt, :published_at, :title, :meta_description, :state]
  end

  def self.date_constraints
    { year: /\d{4}/, month: /\d{1,2}/ }
  end
end
